var postcss = require('postcss');
var fs      = require("fs");

function ends(str){
    return str.endsWith('/');
}

function prefix(str){
    if(ends(str)) {
        return str + '';
    }
    return str + '/';
}

function cacheBuster(){
    return '?' + Math.random(100) * 1000;
}

module.exports = postcss.plugin('htmlCreator', function (opts) {

    opts = opts || {};

    var dest    = opts.dest ? opts.dest : './';
    var css     = opts.css  ? opts.css + cacheBuster() : '';
    var js      = opts.js   ? opts.js + cacheBuster() : '';
    var content = opts.content ? opts.content : '';
    var title   = opts.title ? opts.title : '';
    var outputFile  = prefix(dest) + 'index.html';

    return function () {
        var stream = fs.createWriteStream(outputFile);
        stream.once('open', function(fd) {
            stream.write('<!DOCTYPE html>\n');
            stream.write('<html>\n');
            stream.write('\t<head>\n');
            stream.write('\t\t<title>'+title+'</title>\n');
            stream.write('\t\t<meta charset="utf-8">\n');
            stream.write('\t\t<meta name="viewport" content="width=device-width, initial-scale=1.0">\n');
            stream.write('\t\t<link rel="stylesheet" type="text/css" href="'+css+'">\n');
            stream.write('\t</head>\n');
                stream.write('\t<body>\n');
                    stream.write('\t\t' + content + '\n' );
                    stream.write( '\t\t<script src="'+js+'"></script>\n' );
                stream.write('\t</body>\n');
            stream.write('</html>\n');
            stream.end();
        });
    };
});
